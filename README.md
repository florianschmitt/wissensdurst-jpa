# README - Wissensdurst: Fortschrittliche Java-ORM Lösungen (13.05.15)#
## Florian Schmitt <florian.schmitt@explicatis.com> ##

Spring Boot + Spring Data JPA + Vaadin + gradle example

benutzt Jetty als Webserver, eclipseLink als JPA Provider und MySQL als DBMS

## Voraussetzungen ##
Java 8, gradle 2.+

```
C:\Users\fschmitt>gradle -v

------------------------------------------------------------
Gradle 2.4
------------------------------------------------------------

Build time:   2015-05-05 08:09:24 UTC
Build number: none
Revision:     5c9c3bc20ca1c281ac7972643f1e2d190f2c943c

Groovy:       2.3.10
Ant:          Apache Ant(TM) version 1.9.4 compiled on April 29 2014
JVM:          1.8.0_45 (Oracle Corporation 25.45-b02)
OS:           Windows 7 6.1 amd64
```

### Datenbankeinstellungen einrichten ###
```
Einstellungen sind in src/main/resources/datasource.properties
Default ist MySQL mit url jdbc:mysql://localhost:3306/wissensdurst und Benutzer/Passwort wissensdurst
Entweder Datenbank mit diesen Einstellungen einrichten, oder Werte in datasource.properties anpassen
```

## Befehle ##

```
#!sh
Application booten
$ gradle run
aufrufen unter http://localhost:9999/

Tests ausführen
$ gradle test

Eclipse Projekt erstellen
$ gradle eclipse
```