package ui;

import ui.Broadcaster.Bean;
import ui.Broadcaster.BroadcastListener;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
public class QueryTextReceiverSQLOnly extends HorizontalLayout implements BroadcastListener
{

	private static final Object	GRID_ID_QUERY		= "query";

	private Grid				grid				= new Grid();
	private TextField			queryCountTextfield	= new TextField();

	public QueryTextReceiverSQLOnly()
	{
		Broadcaster.register(this);
		queryCountTextfield.setReadOnly(true);
		grid.removeHeaderRow(0);
		grid.setImmediate(true);
		grid.setSizeFull();
		grid.addColumn(GRID_ID_QUERY)//
				.setMinimumWidth(2000);

		FooterRow fr = grid.appendFooterRow();
		fr.getCell(GRID_ID_QUERY)//
				.setComponent(queryCountTextfield);

		setSizeFull();
		addComponent(grid);
	}

	@Override
	public void detach()
	{
		Broadcaster.unregister(this);
		super.detach();
	}

	public void clear()
	{
		grid.getContainerDataSource().removeAllItems();
	}

	@Override
	public void receiveBroadcast(final Bean message)
	{
		UI.getCurrent().access(() -> {
			grid.addRow(message.getSql());
			grid.scrollToEnd();
			queryCountTextfield.setReadOnly(false);
			queryCountTextfield.setValue(String.valueOf(grid.getContainerDataSource().size()));
			queryCountTextfield.setReadOnly(true);
		});
	}
}
