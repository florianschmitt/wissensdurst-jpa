package ui;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import lombok.NoArgsConstructor;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalSplitPanel;

@SuppressWarnings("serial")
@NoArgsConstructor
@SpringView(name = MainView.VIEW_NAME)
public class MainView extends VerticalSplitPanel implements View
{

	public static final String	VIEW_NAME	= "mainView";

	@Inject
	private Test1				test1;

	@Inject
	private Test2				test2;

	@Inject
	private Test3				test3;

	@Inject
	private Test4				test4;

	private TabSheet			tabSheet	= new TabSheet();

	@PostConstruct
	private void init()
	{
		QueryTextReceiverSQLOnly qtr = new QueryTextReceiverSQLOnly();
		setSizeFull();
		tabSheet.setSizeFull();
		tabSheet.addSelectedTabChangeListener(l -> {
			qtr.clear();
			GridFilterComponent gridFilter = (GridFilterComponent) tabSheet.getSelectedTab();
			gridFilter.refresh();
		});
		tabSheet.addTab(test1, "Test 1");
		tabSheet.addTab(test2, "Test 2");
		tabSheet.addTab(test3, "Test 3");
		tabSheet.addTab(test4, "Test 4");

		setFirstComponent(tabSheet);
		setSecondComponent(qtr);
	}

	@Override
	public void enter(ViewChangeEvent event)
	{
	}
}
