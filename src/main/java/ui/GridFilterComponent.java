package ui;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import jpa.domain.Author;
import jpa.domain.Book;
import jpa.domain.Publisher;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import service.AuthorServiceSpringData;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@RequiredArgsConstructor
@SuppressWarnings("serial")
public abstract class GridFilterComponent extends VerticalLayout
{

	@Inject
	protected AuthorServiceSpringData	authorService;

	@Getter
	protected Grid						grid		= new Grid();

	@Getter
	protected TextField					filter		= new TextField();

	protected BeanItemContainer<Author>	container	= new BeanItemContainer<>(Author.class);

	protected abstract Supplier<List<Author>> findAllAuthorEntities();

	protected abstract Function<String, List<Author>> findAllAuthorLikeEntities();

	@PostConstruct
	private void init()
	{
		addAttachListener(a -> {
			setSizeFull();
			filter.addTextChangeListener(l -> refreshContainer(getEntities(l.getText())));
			grid.setSizeFull();
			grid.setContainerDataSource(getConcreteContainer());
			grid.removeAllColumns();
			grid.addColumn("name");
			grid.addColumn("publisher");
			addComponents(filter, grid);
			setExpandRatio(grid, 1f);
		});
	}

	public void refresh()
	{
		filter.setValue("");
		refreshContainer(getEntities());
	}

	protected List<Author> getEntities()
	{
		return findAllAuthorEntities().get();
	}

	protected List<Author> getEntities(String input)
	{
		return findAllAuthorLikeEntities().apply(input);
	}

	private void refreshContainer(Collection<Author> authors)
	{
		container.removeAllItems();
		container.addAll(authors);
	}

	private GeneratedPropertyContainer getConcreteContainer()
	{
		GeneratedPropertyContainer generatedPropertyContainer = new GeneratedPropertyContainer(container);
		generatedPropertyContainer.addGeneratedProperty("publisher", new PropertyValueGenerator<String>()
		{

			@Override
			public String getValue(Item item, Object itemId, Object propertyId)
			{
				Author author = (Author) itemId;
				String joinedPublishers = author.getBooks().stream()//
						.map(Book::getPublisher)//
						.map(Publisher::getName)//
						.sorted(Comparator.naturalOrder())//
						.distinct()//
						.collect(Collectors.joining(", "));
				return joinedPublishers;
			}

			@Override
			public Class<String> getType()
			{
				return String.class;
			}
		});
		generatedPropertyContainer.addGeneratedProperty("name", new PropertyValueGenerator<String>()
		{

			@Override
			public String getValue(Item item, Object itemId, Object propertyId)
			{
				Author author = (Author) itemId;
				return author.getFirstname() + " " + author.getLastname();
			}

			@Override
			public Class<String> getType()
			{
				return String.class;
			}
		});
		return generatedPropertyContainer;
	}
}
