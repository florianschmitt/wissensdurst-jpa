package ui;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import jpa.domain.Author;

import com.vaadin.spring.annotation.SpringComponent;

@SuppressWarnings("serial")
@SpringComponent
public class Test3 extends GridFilterComponent
{

	@Override
	protected Supplier<List<Author>> findAllAuthorEntities()
	{
		return authorService::findAllFetch;
	}

	@Override
	protected Function<String, List<Author>> findAllAuthorLikeEntities()
	{
		return authorService::findByLastnameLikeFetch;
	}
}
