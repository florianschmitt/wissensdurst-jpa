package ui;

import javax.inject.Inject;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@SpringUI
@PreserveOnRefresh
@Theme("valo")
@Push
public class VaadinUI extends UI
{

	@Inject
	private SpringViewProvider	viewProvider;

	@Override
	protected void init(VaadinRequest vaadinRequest)
	{
		viewProvider.setAccessDeniedViewClass(ErrorView.class);
		Navigator nav = new Navigator(this, this);
		nav.setErrorView(ErrorView.class);
		nav.addProvider(viewProvider);
		nav.navigateTo(MainView.VIEW_NAME);
	}
}