package ui;

import javax.annotation.PostConstruct;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
/**
 * is not injected but instanciated.
 * 
 * @author fschmitt
 *
 */
@SpringComponent
public class ErrorView extends HorizontalLayout implements View
{

	public ErrorView()
	{
		init();
	}

	@PostConstruct
	private void init()
	{
		Label l = new Label("Error: view not found");
		addComponents(l);
	}

	@Override
	public void enter(ViewChangeEvent event)
	{
	}
}
