package ui;

import java.util.Collection;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ui.Broadcaster.Bean;
import ui.Broadcaster.BroadcastListener;

import com.vaadin.data.Item;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
public class QueryTextReceiver extends HorizontalLayout implements BroadcastListener
{

	private static final Object	GRID_ID_QUERY			= "query";
	private static final Object	GRID_ID_EXECUTION_TIME	= "queryExecutionTime";

	private Grid				grid					= new Grid();
	private TextField			queryCountTextfield		= new TextField();
	private TextField			executionTimeTextfield	= new TextField();

	public QueryTextReceiver()
	{
		Broadcaster.register(this);
		queryCountTextfield.setReadOnly(true);
		grid.removeHeaderRow(0);
		grid.setImmediate(true);
		grid.setSizeFull();
		grid.addColumn(GRID_ID_EXECUTION_TIME, Long.class)//
				.setConverter(new Converter<String, Long>()
				{

					@Override
					public Long convertToModel(String value, Class<? extends Long> targetType, Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException
					{
						return null;
					}

					@Override
					public String convertToPresentation(Long value, Class<? extends String> targetType, Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException
					{
						long convert = TimeUnit.MILLISECONDS.convert(value, TimeUnit.NANOSECONDS);
						return String.format("%s ms", String.valueOf(convert));
					}

					@Override
					public Class<Long> getModelType()
					{
						return Long.class;
					}

					@Override
					public Class<String> getPresentationType()
					{
						return String.class;
					}
				});
		grid.addColumn(GRID_ID_QUERY)//
				.setMinimumWidth(2000);

		FooterRow fr = grid.appendFooterRow();
		fr.getCell(GRID_ID_EXECUTION_TIME)//
				.setComponent(queryCountTextfield);

		fr.getCell(GRID_ID_QUERY)//
				.setComponent(executionTimeTextfield);

		setSizeFull();
		addComponent(grid);
	}

	@Override
	public void detach()
	{
		Broadcaster.unregister(this);
		super.detach();
	}

	public void clear()
	{
		grid.getContainerDataSource().removeAllItems();
	}

	private long getCumulatedTime()
	{
		Collection<?> properties = grid.getContainerDataSource().getContainerPropertyIds();
		Object next = properties.iterator().next();
		Collection<?> itemIds = grid.getContainerDataSource().getItemIds();
		long result = 0;
		for (Object id : itemIds)
		{
			Item item = grid.getContainerDataSource().getItem(id);
			long value = (long) item.getItemProperty(next).getValue();
			result += value;
		}
		return result;
	}

	@Override
	public void receiveBroadcast(final Bean message)
	{
		UI.getCurrent().access(() -> {
			grid.addRow(message.getTime(), message.getSql());
			grid.scrollToEnd();
			queryCountTextfield.setReadOnly(false);
			queryCountTextfield.setValue(String.valueOf(grid.getContainerDataSource().size()));
			queryCountTextfield.setReadOnly(true);

			executionTimeTextfield.setReadOnly(false);
			long cumulatedTime = getCumulatedTime();
			long convert = TimeUnit.MILLISECONDS.convert(cumulatedTime, TimeUnit.NANOSECONDS);
			executionTimeTextfield.setValue(String.format("%s ms", String.valueOf(convert)));
			executionTimeTextfield.setReadOnly(true);
		});
	}
}
