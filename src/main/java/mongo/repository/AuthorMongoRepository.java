package mongo.repository;

import java.util.List;

import mongo.domain.AuthorM;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AuthorMongoRepository extends MongoRepository<AuthorM, ObjectId>
{

	public List<AuthorM> findByLastnameIgnoreCase(String input);

	public List<AuthorM> findByLastnameContainingIgnoreCase(String input);

}
