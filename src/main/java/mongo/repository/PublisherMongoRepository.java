package mongo.repository;

import mongo.domain.PublisherM;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PublisherMongoRepository extends MongoRepository<PublisherM, ObjectId>
{

}
