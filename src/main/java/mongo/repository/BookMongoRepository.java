package mongo.repository;

import mongo.domain.BookM;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookMongoRepository extends MongoRepository<BookM, ObjectId>
{

}
