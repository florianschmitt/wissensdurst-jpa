package mongo.domain;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
@Document(collection = "book")
public class BookM implements Serializable
{

	@Id
	private ObjectId		id;

	@Indexed
	private String			title;
	private String			isbn;

	@DBRef
	private List<AuthorM>	authors;

	@DBRef
	private PublisherM		publisher;
}
