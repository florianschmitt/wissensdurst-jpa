package service;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import jpa.domain.Author;
import jpa.domain.Book;
import jpa.domain.Publisher;
import jpa.repository.AuthorRepository;
import jpa.repository.BooksRepository;
import jpa.repository.PublisherRepository;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gwt.thirdparty.guava.common.collect.Lists;

@Component
@Transactional
public class GenerateTestDataService
{

	@Inject
	private AuthorRepository	authorRepository;

	@Inject
	private PublisherRepository	publisherRepository;

	@Inject
	private BooksRepository		booksRepository;

	// @Inject
	// private AuthorMongoRepository authorRepositoryMongo;
	//
	// @Inject
	// private BookMongoRepository bookMongoRepository;
	//
	// @Inject
	// private PublisherMongoRepository publisherMongoRepository;

	@PostConstruct
	public void generateData()
	{
		generatePublisher();
		generateBooks();
		generate1000Authors();
	}

	private void generate1000Authors()
	{
		long count = authorRepository.count();

		if (count > 0)
			return;

		List<String> firstNames = Lists.newArrayList("Franz", "Christa", "Elisa", "Hans");
		List<String> lastNames = Lists.newArrayList("Nagelschmidt", "Mustermann", "Musterfrau", "Müller");

		List<Author> authors = Lists.newLinkedList();

		IntStream.range(0, 1000).forEach(x -> {
			Author p = new Author();
			String firstName = firstNames.get(new Random().nextInt(firstNames.size()));
			String lastName = lastNames.get(new Random().nextInt(lastNames.size()));
			p.setFirstname(firstName);
			p.setLastname(lastName);
			List<Book> findAll = Lists.newArrayList(booksRepository.findAll());
			Collections.shuffle(findAll);
			List<Book> collect = findAll.stream().limit(30).collect(Collectors.toList());
			p.setBooks(collect);
			authors.add(p);
		});
		authorRepository.save(authors);
	}

	private void generatePublisher()
	{
		long count = publisherRepository.count();

		if (count > 0)
			return;

		List<Publisher> publisher = Lists.newLinkedList();

		IntStream.range(0, 100).forEach(x -> {
			Publisher p = new Publisher();
			p.setName("Verlag " + x);
			publisher.add(p);
		});
		publisherRepository.save(publisher);
	}

	private void generateBooks()
	{
		long count = booksRepository.count();

		if (count > 0)
			return;

		List<String> firstNames = Lists.newArrayList("Game of Thrones", "Spider Man", "Buch", "Tatort");
		List<Book> books = Lists.newLinkedList();

		IntStream.range(0, 1000).forEach(x -> {
			Book p = new Book();
			String firstName = firstNames.get(new Random().nextInt(firstNames.size()));
			String isbn = String.valueOf(new Random().nextInt(Integer.MAX_VALUE));
			p.setIsbn(isbn);
			p.setTitle(String.format("%s %d", firstName, x));
			List<Publisher> findAll = Lists.newArrayList(publisherRepository.findAll());
			Collections.shuffle(findAll);
			p.setPublisher(findAll.get(0));
			books.add(p);
		});

		booksRepository.save(books);
	}

	// @PostConstruct
	// public void generateMongoData()
	// {
	// generatePublisherM();
	// generateBooksM();
	// generate1000AuthorsMongo();
	// }
	//
	// private void generate1000AuthorsMongo()
	// {
	// long count = authorRepositoryMongo.count();
	//
	// if (count > 0)
	// return;
	//
	// List<String> firstNames = Lists.newArrayList("Franz", "René", "Mark", "Hans");
	// List<String> lastNames = Lists.newArrayList("Schmitt", "Keller", "Hansen", "M�ller");
	//
	// List<AuthorM> authors = Lists.newLinkedList();
	//
	// IntStream.range(0, 1000).forEach(x -> {
	// AuthorM p = new AuthorM();
	// String firstName = firstNames.get(new Random().nextInt(firstNames.size()));
	// String lastName = lastNames.get(new Random().nextInt(lastNames.size()));
	// p.setFirstname(firstName);
	// p.setLastname(lastName);
	// List<BookM> findAll = Lists.newArrayList(bookMongoRepository.findAll());
	// Collections.shuffle(findAll);
	// List<BookM> collect = findAll.stream().limit(30).collect(Collectors.toList());
	// p.setBooks(collect);
	// authors.add(p);
	// });
	// authorRepositoryMongo.save(authors);
	// }
	//
	// private void generatePublisherM()
	// {
	// long count = publisherMongoRepository.count();
	//
	// if (count > 0)
	// return;
	//
	// List<PublisherM> publisher = Lists.newLinkedList();
	//
	// IntStream.range(0, 100).forEach(x -> {
	// PublisherM p = new PublisherM();
	// p.setName("Publisher " + x);
	// publisher.add(p);
	// });
	// publisherMongoRepository.save(publisher);
	// }
	//
	// private void generateBooksM()
	// {
	// long count = bookMongoRepository.count();
	//
	// if (count > 0)
	// return;
	//
	// List<String> firstNames = Lists.newArrayList("Game of Thrones", "Spider Man", "Buch", "Tatort");
	// List<BookM> books = Lists.newLinkedList();
	//
	// IntStream.range(0, 1000).forEach(x -> {
	// BookM p = new BookM();
	// String firstName = firstNames.get(new Random().nextInt(firstNames.size()));
	// String isbn = String.valueOf(new Random().nextInt(Integer.MAX_VALUE));
	// p.setIsbn(isbn);
	// p.setTitle(String.format("%s %d", firstName, x));
	// List<PublisherM> findAll = Lists.newArrayList(publisherMongoRepository.findAll());
	// Collections.shuffle(findAll);
	// p.setPublisher(findAll.get(0));
	// books.add(p);
	// });
	//
	// bookMongoRepository.save(books);
	// }
}
