package service;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import jpa.domain.Author;
import jpa.domain.QAuthor;
import jpa.domain.QBook;
import jpa.domain.QPublisher;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;

@Component
@Transactional(readOnly = true)
public class AuthorServiceQueryDSL
{

	private final static QAuthor	author	= QAuthor.author;

	@Inject
	private EntityManager			entityManager;

	public List<Author> findAll()
	{
		List<Author> result = new JPAQuery(entityManager)//
				.from(author)//
				.list(author);
		return result;
	}

	public List<Author> findByLastname(String input)
	{
		List<Author> result = new JPAQuery(entityManager)//
				.from(author)//
				.where(author.lastname.equalsIgnoreCase(input))//
				.list(author);
		return result;
	}

	public List<Author> findByLastnameLike(String input)
	{
		List<Author> result = new JPAQuery(entityManager)//
				.from(author)//
				.where(author.lastname.containsIgnoreCase(input))//
				.list(author);
		return result;
	}

	public List<Author> findAdvancedQueryExample(String input)
	{
		QPublisher p = QPublisher.publisher;
		QBook b = QBook.book;

		List<Author> result = new JPAQuery(entityManager)//
				.from(author)//
				.join(author.books, b)//
				.where(b.title.containsIgnoreCase(input))//
				.where(b.publisher.in(new JPASubQuery()//
						.from(p)//
						.where(p.address.contains("GERMANY"))//
						.list(p)))//
				.list(author);
		return result;
	}
}
