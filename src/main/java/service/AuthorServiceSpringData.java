package service;

import java.util.List;

import javax.inject.Inject;

import jpa.domain.Author;
import jpa.domain.QAuthor;
import jpa.repository.AuthorRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class AuthorServiceSpringData
{

	private final static QAuthor	author	= QAuthor.author;

	@Inject
	private AuthorRepository		repository;

	public List<Author> findAll()
	{
		return Lists.newArrayList(repository.findAll());
	}

	public List<Author> findAllLoadPublisher()
	{
		return Lists.newArrayList(repository.findAllBatchFetchBooksAndPublisher());
	}

	public List<Author> findByLastname(String input)
	{
		return repository.findByLastnameIgnoreCase(input);
	}

	public List<Author> findByLastnameLoadPublisher(String input)
	{
		return repository.findByLastnameIgnoreCaseLoadPublisher(input);
	}

	public List<Author> findByLastnameLoadPublisherIn(String input)
	{
		return repository.findByLastnameIgnoreCaseLoadPublisherIn(input);
	}
	
	public List<Author> findAllLoadPublisherExists()
	{
		return repository.findAllBatchFetchBooksAndPublisherBatchTypeExists();
	}

	public List<Author> findByLastnameLoadPublisherExists(String input)
	{
		return repository.findByLastnameIgnoreCaseLoadPublisherExists(input);
	}

	public List<Author> findByLastnameLike(String input)
	{
		return repository.findByLastnameContainingIgnoreCase(input);
	}

	public List<Author> findByLastnameLike2(String input)
	{
		Predicate predicate = author.lastname.containsIgnoreCase(input);
		return Lists.newArrayList(repository.findAll(predicate));
	}

	public List<Author> findByPublisherName(String input)
	{
		return repository.findByPublisherName(input);
	}

	public List<Author> findByLastnameLikeLoadPublisher(String input)
	{
		return repository.findByLastnameContainingIgnoreCaseLoadPublisher(input);
	}

	public List<Author> findByLastnameLikeFetch(String input)
	{
		return repository.findByLastnameIgnoreCaseContainingFetch(input);
	}

	public List<Author> findAllFetch()
	{
		return repository.findAllJoinFetchBooksAndPublisher();
	}

	public List<Author> findByLastnameLikeLoadPublisherIn(String input)
	{
		return repository.findByLastnameContainingIgnoreCaseLoadPublisherIn(input);
	}

	public List<Author> findByLastnameContainingIgnoreCaseLoadPublisherIn(String input)
	{
		return repository.findByLastnameContainingIgnoreCaseLoadPublisherIn(input);
	}

	public List<Author> findAllLoadPublisherIn()
	{
		return repository.findAllBatchFetchBooksAndPublisherBatchTypeIn();
	}

	public List<Author> findByLastnameContainingIgnoreCaseFetchBatch(String input)
	{
		return repository.findByLastnameContainingFetchAndBatch(input);
	}

	public List<Author> findAllLoadPublisherFetchBatch()
	{
		return repository.findAllFetchAndBatch();
	}

	public List<Author> findByLastnameContainingIgnoreCaseFetchBatch2(String input)
	{
		return repository.findByLastnameContainingFetchAndBatch2(input);
	}

	public List<Author> findAllLoadPublisherFetchBatch2()
	{
		return repository.findAllFetchAndBatch2();
	}

	public List<Author> findByLastnameLikeLoadPublisherExists(String input)
	{
		return repository.findByLastnameContainingIgnoreCaseLoadPublisherExists(input);
	}

	public Page<Author> findByLastname(String input, Pageable pageable)
	{
		return repository.findByLastnameContainingIgnoreCase(input, pageable);
	}

	public Page<Author> findByLastnameLoadPublisher(String input, Pageable pageable)
	{
		return repository.findByLastnameContainingIgnoreCaseLoadPublisher(input, pageable);
	}
}
