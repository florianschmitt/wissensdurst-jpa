package service;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.domain.Author;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(readOnly = true)
public class AuthorServicePlainJPA
{

	@Inject
	private EntityManager	entityManager;

	public List<Author> findAll()
	{
		String queryString = "SELECT a FROM Author a";
		TypedQuery<Author> query = entityManager.createQuery(queryString, Author.class);
		return query.getResultList();
	}

	public List<Author> findByLastname(String input)
	{
		String queryString = "SELECT a FROM Author a " //
				+ "WHERE LOWER(a.lastname) = LOWER(:input)";
		TypedQuery<Author> query = entityManager.createQuery(queryString, Author.class);
		query.setParameter("input", input);
		return query.getResultList();
	}

	public List<Author> findByLastnameLike(String input)
	{
		String queryString = "SELECT a FROM Author a " //
				+ "WHERE LOWER(a.lastname) LIKE LOWER(:input)";
		TypedQuery<Author> query = entityManager.createQuery(queryString, Author.class);
		query.setParameter("input", "%" + input + "%");
		return query.getResultList();
	}
}
