//package service;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import mongo.domain.AuthorM;
//import mongo.repository.AuthorMongoRepository;
//
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.google.gwt.thirdparty.guava.common.collect.Lists;
//
//@Service
//@Transactional(readOnly = true)
//public class AuthorServiceMongo
//{
//
//	@Inject
//	private AuthorMongoRepository	authorMongoRepository;
//
//	public List<AuthorM> findAll()
//	{
//		return Lists.newArrayList(authorMongoRepository.findAll());
//	}
//
//	public List<AuthorM> findByLastnameIgnoreCase(String input)
//	{
//		return authorMongoRepository.findByLastnameIgnoreCase(input);
//	}
//
//	public List<AuthorM> findByLastnameContainingIgnoreCase(String input)
//	{
//		return authorMongoRepository.findByLastnameContainingIgnoreCase(input);
//	}
//}
