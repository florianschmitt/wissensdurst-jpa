package util;

import org.springframework.boot.SpringApplication;

public class RunApp
{

	public static void main(String[] args) throws Exception
	{
		SpringApplication.run(SpringConfiguration.class, args);
	}
}
