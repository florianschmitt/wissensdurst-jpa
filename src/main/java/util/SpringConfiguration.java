package util;

import java.util.Map;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.logging.SessionLog;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;

import com.google.common.collect.Maps;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"service", "ui"})
@EntityScan(basePackages = {"jpa.domain"})
@EnableJpaRepositories(basePackages = {"jpa.repository"})
@Import(C3p0DatasourceConfiguration.class)
// @EnableMongoRepositories(basePackages = {"mongo.repository"})
public class SpringConfiguration extends JpaBaseConfiguration
{

	@Override
	protected AbstractJpaVendorAdapter createJpaVendorAdapter()
	{
		EclipseLinkJpaVendorAdapter adapter = new EclipseLinkJpaVendorAdapter();
		return adapter;
	}

	@Override
	protected Map<String, Object> getVendorProperties()
	{
		Map<String, Object> result = Maps.newHashMap();

		result.put(PersistenceUnitProperties.WEAVING, Boolean.TRUE.toString());

		// disable cache
		result.put(PersistenceUnitProperties.CACHE_SHARED_DEFAULT, Boolean.FALSE.toString());

		// custom logger that broadcasts sql queries, disable to enable logging via console
		result.put(PersistenceUnitProperties.LOGGING_LOGGER, EclipselinkCustomLogger.class.getCanonicalName());
		// to log SQL-queries, set to FINE
		result.put(PersistenceUnitProperties.LOGGING_LEVEL, SessionLog.FINE_LABEL);
		result.put(PersistenceUnitProperties.LOGGING_CONNECTION, Boolean.FALSE.toString());
		result.put(PersistenceUnitProperties.LOGGING_PARAMETERS, Boolean.TRUE.toString());
		result.put(PersistenceUnitProperties.LOGGING_SESSION, Boolean.FALSE.toString());
		result.put(PersistenceUnitProperties.LOGGING_THREAD, Boolean.FALSE.toString());
		result.put(PersistenceUnitProperties.LOGGING_TIMESTAMP, Boolean.FALSE.toString());
		// result.put(PersistenceUnitProperties.SESSION_EVENT_LISTENER_CLASS,
		// EclipselinkSessionListener.class.getCanonicalName());
		return result;
	}
}
