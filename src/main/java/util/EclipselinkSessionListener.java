package util;

import org.eclipse.persistence.internal.jaxb.SessionEventListener;
import org.eclipse.persistence.sessions.SessionEvent;

import ui.Broadcaster;
import ui.Broadcaster.Bean;

/**
 * 
 * wird zur Zeit nicht benutzt, da auf diesem Wege nicht alle Queries abgefangen werden.
 * 
 * @author fschmitt
 *
 */
public class EclipselinkSessionListener extends SessionEventListener
{

	private long	startTime;

	@Override
	public void preExecuteQuery(SessionEvent event)
	{
		startTime = System.nanoTime();
	}

	@Override
	public void postExecuteQuery(SessionEvent event)
	{
		long endTime = System.nanoTime();
		long exeTime = endTime - startTime;
		String sqlString = event.getQuery().getSQLString();

		if (sqlString != null)
		{
			Broadcaster.broadcast(new Bean(sqlString, exeTime));
		}
	}
}
