package util;

import org.eclipse.persistence.logging.AbstractSessionLog;
import org.eclipse.persistence.logging.SessionLogEntry;

import ui.Broadcaster;
import ui.Broadcaster.Bean;

import com.google.gwt.thirdparty.guava.common.base.Strings;

public class EclipselinkCustomLogger extends AbstractSessionLog
{

	@Override
	public void log(SessionLogEntry sessionLogEntry)
	{
		String nameSpace = sessionLogEntry.getNameSpace();
		if (!Strings.isNullOrEmpty(nameSpace) && nameSpace.equals("sql"))
		{
			String message = sessionLogEntry.getMessage();
			Broadcaster.broadcast(new Bean(message, 0l));
		}
	}
}
