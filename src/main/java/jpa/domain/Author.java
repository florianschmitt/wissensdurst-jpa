package jpa.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedSubgraph;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.eclipse.persistence.annotations.Index;
import org.springframework.data.jpa.domain.AbstractPersistable;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
@Entity
@NamedEntityGraphs(value = {@NamedEntityGraph(name = "loadWithPublisherName", //
attributeNodes = {@NamedAttributeNode(value = "books", subgraph = "books"),}, //
subgraphs = {//
@NamedSubgraph(name = "books", attributeNodes = {@NamedAttributeNode(subgraph = "publisher", value = "publisher")}), //
		@NamedSubgraph(name = "publisher", attributeNodes = {@NamedAttributeNode("name")})//
})})
public class Author extends AbstractPersistable<Long>
{

	@Index
	private String		firstname;
	@Index
	private String		lastname;
	@ManyToMany
	private List<Book>	books;
}
