package jpa.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.eclipse.persistence.annotations.Index;
import org.springframework.data.jpa.domain.AbstractPersistable;

@SuppressWarnings("serial")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Book extends AbstractPersistable<Long>
{

	@Index
	private String			title;
	private String			isbn;

	@ManyToMany(mappedBy = "books")
	private List<Author>	authors;

	@ManyToOne
	private Publisher		publisher;
}
