package jpa.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.eclipse.persistence.annotations.Index;
import org.springframework.data.jpa.domain.AbstractPersistable;

@SuppressWarnings("serial")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Publisher extends AbstractPersistable<Long>
{

	@Index
	private String		name;
	private String		address;

	@OneToMany(mappedBy = "publisher")
	private List<Book>	books;
}
