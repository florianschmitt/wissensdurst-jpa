package jpa.repository;

import jpa.domain.Book;

import org.springframework.data.repository.CrudRepository;

public interface BooksRepository extends CrudRepository<Book, Long>
{

}
