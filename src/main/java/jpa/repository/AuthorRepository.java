package jpa.repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.persistence.QueryHint;

import jpa.domain.Author;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AuthorRepository extends CrudRepository<Author, Long>, QueryDslPredicateExecutor<Author>
{

	@Query("Select a From Author a")
	public List<Author> findAll();

	public List<Author> findByLastnameIgnoreCase(String input);

	public List<Author> findByLastnameContainingIgnoreCase(String input);

	@Query("Select a From Author a Where Upper(a.lastname) = Upper(:input)")
	public List<Author> findByLastnameIgnoreCaseQuery(@Param("input") String input);

	@Query("Select a From Author a Where Upper(a.lastname) Like Upper(Concat('%', :input, '%'))")
	public List<Author> findByLastnameContainingIgnoreCaseQuery(@Param("input") String input);

	public Page<Author> findByLastnameIgnoreCase(String input, Pageable pageable);

	public Page<Author> findByLastnameContainingIgnoreCase(String input, Pageable pageable);

	@Query("Select a From Author a")
	public Stream<Author> streamAll();

	public Optional<Author> findOneById(long id);

	@Query("Select a From Author a Join a.books b Join b.publisher p where p.name Like :name")
	public List<Author> findByPublisherName(@Param("name") String input);

	@Query("Select a From Author a")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
	})
	public List<Author> findAllBatchFetchBooksAndPublisher();

	@Query("Select a From Author a")
	@QueryHints({//
	@QueryHint(name = "eclipselink.join-fetch", value = "a.books"),//
			@QueryHint(name = "eclipselink.join-fetch", value = "a.books.publisher"),//
	})
	public List<Author> findAllJoinFetchBooksAndPublisher();

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
	})
	public List<Author> findByLastnameContainingIgnoreCaseLoadPublisher(@Param("name") String input);

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "IN")})
	public List<Author> findByLastnameContainingIgnoreCaseLoadPublisherIn(@Param("name") String input);

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "EXISTS")})
	public List<Author> findByLastnameContainingIgnoreCaseLoadPublisherExists(@Param("name") String input);

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints(value = {//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
	}, forCounting = false)
	public Page<Author> findByLastnameContainingIgnoreCaseLoadPublisher(@Param("name") String input, Pageable pageable);

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
	})
	public List<Author> findByLastnameIgnoreCaseLoadPublisher(@Param("name") String input);

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints({//
	@QueryHint(name = "eclipselink.join-fetch", value = "a.books"),//
			@QueryHint(name = "eclipselink.join-fetch", value = "a.books.publisher"),//
	})
	public List<Author> findByLastnameIgnoreCaseContainingFetch(@Param("name") String input);

	@Query("Select a From Author a Where Lower(a.lastname) = Lower(:name)")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "IN")})
	public List<Author> findByLastnameIgnoreCaseLoadPublisherIn(@Param("name") String input);

	@Query("Select a From Author a Where Lower(a.lastname) = Lower(:name)")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "EXISTS")})
	public List<Author> findByLastnameIgnoreCaseLoadPublisherExists(@Param("name") String input);

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "IN")})
	@EntityGraph(value = "loadWithPublisherName", type = EntityGraphType.LOAD)
	public List<Author> findByLastnameContainingIgnoreCaseLoadPublisherInWithEntityGraph(@Param("name") String input);

	@Query("Select a From Author a")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "IN"),//
	})
	public List<Author> findAllBatchFetchBooksAndPublisherBatchTypeIn();

	@Query("Select a From Author a")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "EXISTS"),//
	})
	public List<Author> findAllBatchFetchBooksAndPublisherBatchTypeExists();

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.join-fetch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "IN"),//
	})
	public List<Author> findByLastnameContainingFetchAndBatch(@Param("name") String input);

	@Query("Select a From Author a")
	@QueryHints({//
	@QueryHint(name = "eclipselink.batch", value = "a.books"),//
			@QueryHint(name = "eclipselink.join-fetch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "IN"),//
	})
	public List<Author> findAllFetchAndBatch();

	@Query("Select a From Author a Where Lower(a.lastname) Like Lower(Concat('%', :name, '%'))")
	@QueryHints({//
	@QueryHint(name = "eclipselink.join-fetch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "IN"),//
	})
	public List<Author> findByLastnameContainingFetchAndBatch2(@Param("name") String input);

	@Query("Select a From Author a")
	@QueryHints({//
	@QueryHint(name = "eclipselink.join-fetch", value = "a.books"),//
			@QueryHint(name = "eclipselink.batch", value = "a.books.publisher"),//
			@QueryHint(name = org.eclipse.persistence.config.QueryHints.BATCH_TYPE, value = "IN"),//
	})
	public List<Author> findAllFetchAndBatch2();
}
