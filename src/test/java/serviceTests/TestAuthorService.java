package serviceTests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import jpa.domain.Author;
import jpa.domain.Book;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import service.AuthorServicePlainJPA;
import service.AuthorServiceQueryDSL;
import service.AuthorServiceSpringData;
import util.SpringConfiguration;

@SuppressWarnings("unused")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = {SpringConfiguration.class})
public class TestAuthorService
{

	@Inject
	private AuthorServiceSpringData	authorServiceSpringData;

	@Inject
	private AuthorServicePlainJPA	authorServicePlainJPA;

	@Inject
	private AuthorServiceQueryDSL	authorServiceQueryDSL;

	@Test
	public void testFindByLastname()
	{
		String lastname = "Schmitt";

		List<Author> findByLastName = authorServicePlainJPA.findByLastname(lastname);
		List<Author> findByLastNameQueryDSL = authorServiceQueryDSL.findByLastname(lastname);
		List<Author> findByLastNameSpringData = authorServiceSpringData.findByLastname(lastname);
		testEqualsLists(findByLastName, findByLastNameQueryDSL, findByLastNameSpringData);
	}

	@Test
	public void testFindByLastnameList()
	{
		String lastname = "mi";

		List<Author> findByLastName = authorServicePlainJPA.findByLastnameLike(lastname);
		List<Author> findByLastNameQueryDSL = authorServiceQueryDSL.findByLastnameLike(lastname);
		List<Author> findByLastNameSpringData = authorServiceSpringData.findByLastnameLike(lastname);
		List<Author> findByLastNameSpringData2 = authorServiceSpringData.findByLastnameLike2(lastname);
		testEqualsLists(findByLastName, findByLastNameQueryDSL, findByLastNameSpringData2, findByLastNameSpringData);
	}

	@Test
	public void testFindByPublisherName()
	{
		String name = "Publisher 2";
		authorServiceSpringData.findByPublisherName(name);
	}

	@Test
	public void testFindAllLikeLastname()
	{
		List<Author> findAll = authorServiceSpringData.findAll();
		for (Author a : findAll)
		{
			List<Book> books = a.getBooks();
			for (Book b : books)
			{
				b.getPublisher();
			}
		}
	}

	@Test
	public void testFindAll()
	{
		List<Author> findAll = authorServiceSpringData.findAll();
		for (Author a : findAll)
		{
			List<Book> books = a.getBooks();
			for (Book b : books)
			{
				b.getPublisher();
			}
		}
	}

	@Test
	public void testFindAllLoadPublisher()
	{
		List<Author> findAll = authorServiceSpringData.findAllLoadPublisher();
		for (Author a : findAll)
		{
			List<Book> books = a.getBooks();
			for (Book b : books)
			{
				b.getPublisher();
			}
		}
	}

	@Test
	public void testFindAllLoadPublisherPageable()
	{
		PageRequest pageRequest = new PageRequest(0, 10, new Sort(Direction.ASC, "firstname"));
		Page<Author> firstPage = authorServiceSpringData.findByLastname("mit", pageRequest);
		List<Author> content = firstPage.getContent();
		if (firstPage.hasNext()) {
			Pageable nextPageable = firstPage.nextPageable();
			Page<Author> nextPage = authorServiceSpringData.findByLastname("mit", nextPageable);
		}
	}

	@Test
	public void testFindAllLoadPublisherPageable2()
	{
		boolean hasMoreElements = true;
		Pageable pageRequest = new PageRequest(0, 1000, new Sort(Direction.ASC, "firstname"));
		while (hasMoreElements)
		{
			Page<Author> findAll = authorServiceSpringData.findByLastnameLoadPublisher("mit", pageRequest);
			for (Author a : findAll)
			{
				List<Book> books = a.getBooks();
				for (Book b : books)
				{
					b.getPublisher();
				}
			}
			hasMoreElements = findAll.hasNext();
			if (hasMoreElements)
				pageRequest = findAll.nextPageable();
		}
	}

	@Test
	public void testFindAllLoadPublisherPageable3()
	{
		boolean hasMoreElements = true;
		Pageable pageRequest = new PageRequest(0, 1000, new Sort(Direction.ASC, "firstname"));
		while (hasMoreElements)
		{
			Page<Author> findAll = authorServiceSpringData.findByLastname("mit", pageRequest);
			for (Author a : findAll)
			{
				List<Book> books = a.getBooks();
				for (Book b : books)
				{
					b.getPublisher();
				}
			}
			hasMoreElements = findAll.hasNext();
			if (hasMoreElements)
				pageRequest = findAll.nextPageable();
		}
	}

	private void testEqualsLists(List<Author> l1, List<Author> l2, List<Author> l3)
	{
		assertEquals(l1, l2);
		assertEquals(l2, l3);
	}

	private void testEqualsLists(List<Author> l1, List<Author> l2, List<Author> l3, List<Author> l4)
	{
		assertEquals(l1, l2);
		assertEquals(l2, l3);
		assertEquals(l3, l4);
	}

	private void testEqualsLists(List<Author> l1, List<Author> l2, List<Author> l3, List<Author> l4, List<Author> l5)
	{
		assertEquals(l1, l2);
		assertEquals(l2, l3);
		assertEquals(l3, l4);
		assertEquals(l4, l5);
	}
}
