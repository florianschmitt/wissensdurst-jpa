package wissensdurst;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import jpa.domain.Author;
import jpa.domain.Book;
import jpa.domain.Publisher;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import service.AuthorServiceSpringData;
import util.SpringConfiguration;

import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.google.gwt.thirdparty.guava.common.collect.Lists;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = {SpringConfiguration.class})
@BenchmarkOptions(benchmarkRounds = 2, warmupRounds = 1, concurrency = BenchmarkOptions.CONCURRENCY_SEQUENTIAL)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCasesFindAllLike extends AbstractBenchmark
{

	@Inject
	private AuthorServiceSpringData	authorService;

	protected List<String> getLastnameLikeInputs()
	{
		List<String> lastnames = Lists.newArrayList("Sch", "Mü", "Mu", "st");
		return lastnames;
	}

	private void consumeAuthorList(List<Author> authors)
	{
		for (Author author : authors)
		{
			@SuppressWarnings("unused")
			String joinedPublishers = author.getBooks().stream()//
					.map(Book::getPublisher)//
					.map(Publisher::getName)//
					.sorted(Comparator.naturalOrder())//
					.distinct()//
					.collect(Collectors.joining(", "));
		}
	}

	private void executeTestCase(Function<String, List<Author>> findLike)
	{
		for (String input : getLastnameLikeInputs())
		{
			List<Author> list = findLike.apply(input);
			consumeAuthorList(list);
		}
	}

	@Test
	public void testCase1Like()
	{
		executeTestCase(authorService::findByLastnameLike);
	}

	@Test
	public void testCase2Like()
	{
		executeTestCase(authorService::findByLastnameLoadPublisher);
	}

	@Test
	public void testCase3Like()
	{
		executeTestCase(authorService::findByLastnameLikeFetch);
	}

	@Test
	public void testCase4Like()
	{
		executeTestCase(authorService::findByLastnameContainingIgnoreCaseLoadPublisherIn);
	}

//	@Test
//	public void testCaseXLike()
//	{
//		executeTestCase(authorService::findByLastnameLikeLoadPublisherExists);
//	}
}
