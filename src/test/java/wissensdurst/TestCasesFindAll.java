package wissensdurst;

import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.inject.Inject;

import jpa.domain.Author;
import jpa.domain.Book;
import jpa.domain.Publisher;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import service.AuthorServiceSpringData;
import util.SpringConfiguration;

import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = {SpringConfiguration.class})
@BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 5, concurrency = BenchmarkOptions.CONCURRENCY_SEQUENTIAL)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCasesFindAll extends AbstractBenchmark
{

	@Inject
	private AuthorServiceSpringData	authorService;

	private void consumeAuthorList(List<Author> authors)
	{
		for (Author author : authors)
		{
			@SuppressWarnings("unused")
			String joinedPublishers = author.getBooks().stream()//
					.map(Book::getPublisher)//
					.map(Publisher::getName)//
					.sorted(Comparator.naturalOrder())//
					.distinct()//
					.collect(Collectors.joining(", "));
		}
	}

	private void executeTestCase(Supplier<List<Author>> findAll)
	{
		List<Author> list = findAll.get();
		consumeAuthorList(list);
	}

	@Test
	public void testCase1()
	{
		executeTestCase(authorService::findAll);
	}

	@Test
	public void testCase2()
	{
		executeTestCase(authorService::findAllLoadPublisher);
	}

	@Test
	public void testCase3()
	{
		executeTestCase(authorService::findAllFetch);
	}

	@Test
	public void testCase4()
	{
		executeTestCase(authorService::findAllLoadPublisherIn);
	}
	
	@Test
	public void testCaseX()
	{
		executeTestCase(authorService::findAllLoadPublisherIn);
	}
}
