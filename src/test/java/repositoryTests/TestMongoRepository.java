//package repositoryTests;
//
//import static org.junit.Assert.assertEquals;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import jpa.domain.Author;
//import mongo.repository.AuthorMongoRepository;
//import mongo.repository.PublisherMongoRepository;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.SpringApplicationContextLoader;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import util.VaadinJpaConfiguration;
//
//@SuppressWarnings("unused")
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = {VaadinJpaConfiguration.class})
//public class TestMongoRepository
//{
//
//	@Inject
//	private AuthorMongoRepository		authorRepository;
//
//	@Inject
//	private PublisherMongoRepository	publisherMongoRepository;
//
//	@Test
//	public void testFindLastname()
//	{
//		authorRepository.findAll();
//		publisherMongoRepository.findAll();
//		// List<String> lastnames = Lists.newArrayList("Sch", "hAn", "kEl", "Kel", "kel");
//		// for (String lastname : lastnames)
//		// {
//		// List<Author> findByLastnameLike = authorRepository.findByLastnameContainingIgnoreCase(lastname);
//		// List<Author> findByLastnameLike2 = authorRepository.findByLastnameContainingIgnoreCaseQuery(lastname);
//		// testEqualsLists(findByLastnameLike, findByLastnameLike2);
//		// }
//	}
//
//	private void testEqualsLists(List<Author> l1, List<Author> l2)
//	{
//		assertEquals(l1, l2);
//	}
//
//	private void testEqualsLists(List<Author> l1, List<Author> l2, List<Author> l3)
//	{
//		assertEquals(l1, l2);
//		assertEquals(l2, l3);
//	}
//
//	private void testEqualsLists(List<Author> l1, List<Author> l2, List<Author> l3, List<Author> l4)
//	{
//		assertEquals(l1, l2);
//		assertEquals(l2, l3);
//		assertEquals(l3, l4);
//	}
//
//	private void testEqualsLists(List<Author> l1, List<Author> l2, List<Author> l3, List<Author> l4, List<Author> l5)
//	{
//		assertEquals(l1, l2);
//		assertEquals(l2, l3);
//		assertEquals(l3, l4);
//		assertEquals(l4, l5);
//	}
//}
